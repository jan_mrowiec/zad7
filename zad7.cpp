//https://bitbucket.org/jan_mrowiec/zad7/src/master/zad7.cpp
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <initializer_list>

using namespace std;

class Matrix
{
    static const int size = 3;//rozmiar macierzy
    float** m_dane;//tablica glowna
public:
    //getery setery
    float& get(int i, int j)const
    {
        return m_dane[i][j];
    }
    void set(int i, int j, float v)
    {
        if(i < 0 || i >= size || j < 0 || j >= size)
            return;
        m_dane[i][j] = v;
    }
    //----------------------------------------
    //konstruktory
    Matrix(int d)//wpisuje w kazde pole wartosc d
    {
        m_dane = new float*[size];
        for(int i = 0; i < size; i++)
        {
            *(m_dane + i) = new float[size];
            for(int j = 0; j < size; j++)
                m_dane[i][j] = d;
        }
    }
    Matrix(): Matrix(0)//wpisuje w kazde pole wartosc 0
    {
    }
    Matrix(float** m_dane):m_dane(m_dane) {}
    Matrix(const initializer_list <float> &list)
    {
        initializer_list<float>::iterator it = list.begin();
        m_dane = new float*[size];
        for(int i = 0; i < size; i++)
        {
            *(m_dane + i) = new float[size];
            for(int j = 0; j < size; j++)
                m_dane[i][j] = *(it++);
        }
    }
    Matrix(const Matrix& other)//konstuktor kopiujacy
    {
        m_dane = new float*[size];
        for(int i = 0; i < size; i++)
        {
            *(m_dane + i) = new float[size];
            for(int j = 0; j < size; j++)
                m_dane[i][j] = other.get(i, j);
        }
    }
    ~Matrix()//czyszczenie pamieci
    {
        for(int i = 0; i < size; i++)
            delete [] *(m_dane + i);
        delete [] m_dane;
    }
    //-----------------------------------------
    //operatory
    Matrix& operator=(const Matrix& other)
    {
        for(int i = 0; i < size; i++)
            for(int j = 0; j < size; j++)
                m_dane[i][j] = other.get(i, j);
    }
    Matrix operator+(const Matrix& first)
    {
        float** out;
        out = new float*[size];
        for(int i = 0; i < size; i++)
        {
            *(out + i) = new float[size];
            for(int j = 0; j < size; j++)
                out[i][j] = m_dane[i][j] + first.get(i, j);
        }
        return Matrix(out);
    }
    Matrix operator-(const Matrix& first)
    {
        float** out;
        out = new float*[size];
        for(int i = 0; i < size; i++)
        {
            *(out + i) = new float[size];
            for(int j = 0; j < size; j++)
                out[i][j] =  m_dane[i][j] - first.get(i, j);
        }
        return Matrix(out);
    }
    Matrix& operator+=(const Matrix& other)
    {
        for(int i = 0; i < size; i++)
        {
            for(int j = 0; j < size; j++)
                m_dane[i][j] += other.get(i, j);
        }
        return *this;
    }
    Matrix& operator-=(const Matrix& other)
    {
        for(int i = 0; i < size; i++)
        {
            for(int j = 0; j < size; j++)
                m_dane[i][j] -= other.get(i, j);
        }
        return *this;
    }
    Matrix operator*(const Matrix& other)
    {
        float** out;
        out = new float*[size];
        for(int i = 0; i < size; i++)
        {
            *(out + i) = new float[size];
            for(int j = 0; j < size; j++)
            {
                out[i][j] = 0;
                for(int k = 0, l = 0; k < size; k++, l++)
                    out[i][j] += m_dane[i][k] * other.get(l,j);
            }
        }
        return Matrix(out);
    }
    Matrix& operator*=(const Matrix& other)
    {
        *this = *this * other;
        return *this;
    }
    Matrix operator*(const int r)
    {
        float** out;
        out = new float*[size];
        for(int i = 0; i < size; i++)
        {
            *(out + i) = new float[size];
            for(int j = 0; j < size; j++)
                out[i][j] = m_dane[i][j] * r;
        }
        return Matrix(out);
    }
    Matrix& operator*=(const int r)
    {
        for(int i = 0; i < size; i++)
        {
            for(int j = 0; j < size; j++)
                m_dane[i][j] *= r;
        }
        return *this;
    }
    void Transfer()//transponowanie macierzy - kazdy wiersz z elementami powyzej diagonali zamienia, z adekwatna kolumna ponizej diagonali
    {
        for(int i = 0; i < size; i++)
            for(int j = i + 1; j < size; j++)
            {
                float h = m_dane[i][j];
                m_dane[i][j] = m_dane[j][i];
                m_dane[j][i] = h;
            }
    }
    void c_Idenity()//tworzy macierz jednestkowa
    {
        *this = Matrix();//wyzerowania macierzy
        for(int i = 0; i < size; i++)//nadpisanie diagonali
            m_dane[i][i] = 1;
    }
    float det()const//policzanie wyznacznika - (sprowadzenie macierzy do postaci trojkatnej a nstepnie wymnozenie wartosci na diagonli
    {
        Matrix det = *this;//macierz pomocnicza
        for(int i = 0; i < size ; i++)//dla kazdego wiersza
            for(int j = i + 1; j < size; j++)//dla kazdego wiersza ponizej wiersza j - wyzereuj wartosci pod kolumna i
            {
                if(det.m_dane[i][i] == 0 && det.m_dane[j][i] == 0)
                    continue;
                if(det.m_dane[i][i] == 0)
                {
                    for(int k = i; k < size; k++)
                        det.m_dane[i][k] += det.m_dane[j][k];
                }
                float del = det.m_dane[j][i] / det.m_dane[i][i];
                for(int k = i; k < size; k++)
                    det.m_dane[j][k] -= del * det.m_dane[i][k];

            }
        float out = 1; //wartosc zwracana
        for(int i = 0; i < size; i++)//wymnozenie wartosci na diagonali
            out *= det.m_dane[i][i];
        return out;
    }
    //---------------------------------------------
    //operatory porownania. Dla operatorow mniejszczosc i wiekszosci wynik jest zwracany zalezenie od wyznacznika. Dla operatora rownosci - porownywany jest kazdy element ze soba i na tej podstawie zwracany wynik.
    bool operator==(const Matrix& other)
    {
        for(int i = 0; i < size; i++)
            for(int j = 0; j < size; j++)
                if(m_dane[i][j] != other.get(i, j))
                    return 0;
        return 1;
    }
    bool operator!=(const Matrix& other)
    {
        return !(*this == other);
    }
    bool operator<(const Matrix& other)
    {
        float t_det = det();
        float o_det = other.det();
        if(t_det < o_det)
            return 1;
        else
            return 0;
    }
    bool operator>(const Matrix& other)
    {
        float t_det = det();
        float o_det = other.det();
        if(t_det > o_det)
            return 1;
        else
            return 0;
    }
    bool operator<=(const Matrix& other)
    {
        float t_det = det();
        float o_det = other.det();
        if(t_det <= o_det)
            return 1;
        else
            return 0;
    }
    bool operator>=(const Matrix& other)
    {
        float t_det = det();
        float o_det = other.det();
        if(t_det >= o_det)
            return 1;
        else
            return 0;
    }
    //---------------------------------------------
    //operatory inkremetnacji i dekrementacji
    Matrix operator++(int)
    {
        Matrix temp;
        for(int i = 0; i < size; i++)
            for(int j = 0; j < size; j++)
            {
                temp.m_dane[i][j] = m_dane[i][j]++;
            }
        return temp;
    }
    Matrix operator--(int)
    {
        Matrix temp;
        for(int i = 0; i < size; i++)
            for(int j = 0; j < size; j++)
            {
                temp.m_dane[i][j] = m_dane[i][j]--;
            }
        return temp;
    }
    Matrix& operator++()
    {
        for(int i = 0; i < size; i++)
            for(int j = 0; j < size; j++)
            {
                ++m_dane[i][j];
            }
        return *this;
    }
    Matrix& operator--()
    {
        for(int i = 0; i < size; i++)
            for(int j = 0; j < size; j++)
            {
                --m_dane[i][j];
            }
        return *this;
    }
    friend Matrix operator*(const int r, Matrix& matrix);
    friend ostream& operator<<(ostream& out, const Matrix& matrix);
    friend istream& operator>>(istream& in, Matrix& matrix);
};
Matrix operator*(const int r, Matrix& matrix)
{
    return matrix * r;
}
//------------------------------------------------
//operatory wypisania
ostream& operator<<(ostream& out, const Matrix& matrix)
{
    cout.precision(2);
    for(int i = 0; i < Matrix::size; i++)
    {
        for(int j = 0; j < Matrix::size; j++)
            out << setw(8) << fixed << matrix.m_dane[i][j] << ' ';
        out << endl;
    }
    cout.precision(6);
    return out;
}
istream& operator>>(istream& in, Matrix& matrix)
{
    for(int i = 0; i < Matrix::size; i++)
        for(int j = 0; j < Matrix::size; j++)
            in >> matrix.m_dane[i][j];
    return in;
}
int main()
{
    int ch;
    cout <<"[1] Przykladowe wywolanie" << endl;
    cout <<"[2] Wlasne wywolanie" << endl;
    cin >> ch;
    if(ch != 2)
    {
        Matrix A = {2, 12.5, 1, 13, 1.5, 13, 8, 25, 6};
        cout << "Poczatkowy test" << endl;
        cout << "Twoja macierz" << endl << A;
        cout << "-----------------------------------" << endl;
        cout << "Wyznacznik macierzy " << setprecision(2) << A.det() << endl;
        cout << "-----------------------------------" << endl;
        cout << "Macierz transponowana" << endl;
        Matrix B = A;
        B.Transfer();
        cout << B;
        cout << "-----------------------------------" << endl;
        cout << "Przemnozenie przez skalar - 4" << endl;
        cout << 4 * A << endl;
        cout << "-----------------------------------" << endl;
        cout << "-----------------------------------" << endl;
        cout << "Test dzialan na macierzach" << endl;
        A = {2, 12.5, 1, 13, 1.5, 13, 8, 25, 6};
        B = {1, 0, 1, 1, 4, 2, 0, 1, 2};
        cout << "Macierz A" << endl << A << "Macierz B" << endl << B;
        cout << "Dodawanie macierzy A + B" << endl << A + B << endl;
        cout << "Odjemowanie macierzy A - B" << endl << A - B << endl;
        cout << "Mnozenie macierzy: A * B" << endl << A * B << endl;
        cout << "Mnozenie macierzy: B * A" << endl << B * A << endl;
        cout << "-----------------------------------" << endl;
        cout << "-----------------------------------" << endl;
        cout << "Test sortowanie (wzgledem wyznacznika)" << endl;
        Matrix m[3];
        m[0] = B;
        m[1] = A;
        m[2] = A;
        m[2].c_Idenity();
        cout << "Istnieja trzy tablice" << endl;
        for(int i = 0; i < 3; i++)
            cout << "Tablica " << i << " wskaznik " << setprecision(2) << m[i].det() << endl << m[i];
        sort(m, m  + 3);
        cout << "Tablice po posortowaniu" << endl;
        for(int i = 0; i < 3; i++)
            cout << "Tablica " << i << " wskaznik " << setprecision(2) << m[i].det() << endl << m[i];
    }
    else
    {   Matrix A;
        Matrix B;
        while(1)
        {
            cout << "-----------------------------------" << endl;
            cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
            cout << "-----------------------------------" << endl;
            cout << "Twoja macierz - wyznacznik: " << setprecision(2) << A.det() << endl;
            cout << A;
            cout << "-----------------------------------" << endl;
            cout << "[0]Wprwoadz swoja macierz" << endl;
            cout << "[1]Ustaw element macierzy" << endl;
            cout << "[2]Stworz macierz jednostkowa" << endl;
            cout << "[3]Transpanuj macierz" << endl;
            cout << "[4]Przemnoz przez skalar" << endl;
            cout << "[5]Inkremtuj lub zdekrementuj macierz (zmien wartosc kazdego elementu o 1)" << endl;
            cout << "[6]Dodaj macierze" << endl;
            cout << "[7]Odejmnij macierze" << endl;
            cout << "[8]Przemnoz macierze" << endl;
            cout << "[9]Wyjdz" << endl;
            cout << "Wybrana opcja...";
            cin >> ch;
            switch(ch)
            {
            case 0:
                cout << "Wprowadz wartosci wierszami" << endl;
                cin >> A;
                break;
            case 1:
                int i, j;
                float v;
                cout << "Indeks wiersza: ";
                cin >> i;
                cout << endl << "Indeks kolumny: ";
                cin >> j;
                cout << endl << "Wprowadz nowa wartosc ";
                cin >> v;
                cout << endl;
                A.set(i, j, v);
                break;
            case 2:
                A.c_Idenity();
                break;
            case 3:
                A.Transfer();
                break;
            case 4:
                int s;
                cout << "Wprwoadz skalar ";
                cin >> s;
                A *= s;
                break;
            case 5:
                cout << "[1]Preikrementacja" << endl;
                cout << "[2]Postikrementacja" << endl;
                cout << "[3]Predeikrementacja" << endl;
                cout << "[4]Postdeikrementacja" << endl;
                cout << "Wprwadz opcje ";
                cin >> ch;
                cout << endl;
                switch(ch)
                {
                case 1:
                    cout << "Wypisanie preikrementacji" << endl << ++A;
                    break;
                case 2:
                    cout << "Wypisanie postikrementacji (wartosc zostanie zaktualizowana przy nastepnym wypisaniu)" << endl << A++;
                    break;
                case 3:
                    cout << "Wypisanie predeikrementacji" << endl << --A;
                    break;
                case 4:
                    cout << "Wypisanie postdeiikrementacji (wartosc zostanie zaktualizowana przy nastepnym wypisaniu)" << endl << A--;
                    break;
                }
                break;
            case 6:
                cout << "Wprwoadz druga macierz" << endl;
                cin >> B;
                cout << "Wpisana macierz" << endl;
                cout << B;
                A += B;
                cout << "Macierze zostaly do siebie dodane" << endl;
                break;
            case 7:
                cout << "Wprwoadz druga macierz" << endl;
                cin >> B;
                cout << "Wpisana macierz" << endl;
                cout << B;
                A -= B;
                cout << "Wpisana macierz zostala odjeta od macierzy glownej" << endl;
                break;
            case 8:
                cout << "Wprwoadz druga macierz" << endl;
                cin >> B;
                cout << "Wpisana macierz" << endl;
                cout << B;
                A *= B;
                cout << "Wpisana macierz zostala przemnozana lewostronnie przez macierz glowna" << endl;
                break;
            case 9:
                return 0;
            }
        }
    }
    return 0;
}
